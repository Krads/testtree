import { combineReducers } from 'redux';

import tree from './treeReducer';

export default combineReducers({tree});
