import { connect } from 'react-redux';
import TreeComponent from '../components/TreeComponent';
import { addNode, deleteNode, changeNodeName } from '../actions';

const mapStateToProps = state => ({
  nodes: state.tree.nodes
});

const mapDispatchToProps = dispatch => ({
  addNode(payload) {dispatch(addNode(payload))},
  deleteNode(payload) {dispatch(deleteNode(payload))},
  changeNodeName(payload) {dispatch(changeNodeName(payload))},
})

export default connect(mapStateToProps, mapDispatchToProps)(TreeComponent);