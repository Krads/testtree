export const addNode = payload => ({
  type: 'tree/addNode',
  nodeId: payload
});

export const deleteNode = payload => ({
  type: 'tree/deleteNode',
  nodeId: payload
});

export const changeNodeName = payload => ({
  type: 'tree/changeNodeName',
  idAndName: payload
});
