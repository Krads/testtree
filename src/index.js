import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import RootContainer from './containers/RootContainer';
import registerServiceWorker from './registerServiceWorker';
import configureStore from './store';

const store = configureStore();

ReactDOM.render(<RootContainer store={store} />, document.getElementById('root'));
registerServiceWorker();
