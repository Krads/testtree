const initialState = {
  nodes: {
    1: {
      name: '',
      children: []
    }
  },
}

export default function(state = initialState, payload) {
  switch (payload.type) {
    case 'tree/addNode': {
      const { nodeId } = payload;
      const childId = `${nodeId}${Math.round(Math.random() * 100)}`;
      const childNode = {
        name: '',
        children: [],
        parentId: nodeId
      };
      const newParentNode = { ...state.nodes[nodeId], children: state.nodes[nodeId].children.concat(childId) }
      const newStateNodes = {
        ...state.nodes,
        [nodeId]: newParentNode,
        [childId]: childNode
      }
      return {
        ...state,
        nodes: newStateNodes
      };
    }
    case 'tree/deleteNode': {
      const { nodeId } = payload;
      console.log(state, payload);
      const parentNodeId = state.nodes[nodeId].parentId;
      const newStateNodes = Object.assign({}, state.nodes);
      delete newStateNodes[nodeId];
      newStateNodes[parentNodeId].children = newStateNodes[parentNodeId].children.filter(child => child !== nodeId);
      return {
        ...state,
        nodes: newStateNodes
      }
    }
    case 'tree/changeNodeName': {
      console.log(payload);
      const { idAndName } = payload;
      return {...state,
        nodes: {
        ...state.nodes,
        [idAndName.id]: {
          ...state.nodes[idAndName.id],
          name: idAndName.name}
        }
      }
    }
    default:
      return state;
  }
}
