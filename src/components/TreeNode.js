import React, { Component } from 'react';

export default class TreeNode extends Component {
  onNodeAdd = () => {
    this.props.onAdd(this.props.nodeId);
  };

  onNodeDelete = () => {
    this.props.onDelete(this.props.nodeId);
  };

  onNodeNameChange = event => {
    const name = event.target.value;
    this.props.onChange({id: this.props.nodeId, name});
  };

  render() {
    return (
      <div>
        <button onClick={this.onNodeAdd}>add node</button>
        <input
          type="text"
          value={this.props.node.name}
          onChange={this.onNodeNameChange}
        />
        <button onClick={this.onNodeDelete}>delete node</button>
        <div className="treeNodeChildrenContainer">
          {this.props.children}
        </div>
      </div>
    );
  }
}
