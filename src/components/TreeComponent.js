import React, { Component } from 'react';
import TreeNode from './TreeNode';

export default class TreeComponent extends Component {

  mapNodes = nodeKey => {
    const { nodes } = this.props;
    return (
      <TreeNode
        nodeId={nodeKey}
        key={nodeKey}
        node={nodes[nodeKey]}
        onAdd={this.props.addNode}
        onDelete={this.props.deleteNode}
        onChange={this.props.changeNodeName}
        children={nodes[nodeKey].children.map(this.mapNodes)}
      />
    )
  };

  render() {
    return (
      <div>
        <button onClick={this.props.addNode.bind(null, 1)}>add node</button>
        {this.props.nodes[1].children.map(this.mapNodes)}
      </div>
    );
  }
}
